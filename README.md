# noMnoMnoM, Tweets are tasty.

The goal is to download tweets, to analyse them.
Keywords are: 'Python', 'Data Science', 'archive tweets'.

You can download tweets via the (official) Twitter API, The limitations are:
- The number of tweets returned is limited (unless you pay for a premium developer account).
- Only access when you create a developer account.
- You need to identify yourself when creating a developer account.
- The returned tweets will not be older than a week.
- Some say the twitter API is difficult.

For who is interested in more details:
1. [Twitter Rate Limits @ developer.twitter.com](https://developer.twitter.com/en/docs/rate-limits)
1. [Twitter Pricing 3-day search @ developer.twitter.com](https://developer.twitter.com/en/pricing/search-30day)
1. [Different data between premium and standard searches @ Twitter Community forum](https://twittercommunity.com/t/different-data-between-premium-and-standard-searches/132401/4)

You can download tweets via [Tweepy](#tweepy), 
this is more easy than the Twitter API.
Tweepy is a wrapper around the official Twitter API,
thus it has all the other Twitter API downsides.

__In this example we use the tool called 'snscrape'.__

Snscrape is a tool created to download information from social networks via scraping tools.

Snscrape works around rate limits (since it uses beautiful soup).
See this [discussion][sn04] on the Github project of snscrape, for more explanation..

---

**This page contains**

[[_TOC_]]


## [1] Install/upgrade Python
The scraper need version 3.8 of python.
Check your version via: ```$python3 --version```

### [1.A] Upgrade python on Linux
When needed upgrade to version 3.8.

The instruction below are retrieved via 
[How to run Python on Chromebook @ Chrome Ready][py01] and via 
[Installing Python 3 on Linux @ Python Guide][py02].

Note: The repository 'deadsnakes' is the official python repository. 
Always be smart which repositories you add manually.

[py01]: https://chromeready.com/3079/how-to-run-python-on-chromebook/
[py02]: https://docs.python-guide.org/starting/install3/linux/

```bash
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install python3.8
python3 --version;
```
### [1.B] Upgrade python on chromebook
For a chromebook upgrade does work a bit differently 
since a chromebook has a limited support via the repositories. 

To go around this you need to download and compile the sources yourself.

The instruction below are retrieved via 
[Upgrade to Python 3.8 on a Chromebook by Matt Gosden][cb01], 
[Setup a Chromebook for web development by Matt Gosden][cb02] and via 
[How to make python3 command run Python 3.6 instead of 3.5? @ Stack Overflow][cb03].

[cb01]: https://mattgosden.medium.com/upgrade-to-python-3-8-on-a-chromebook-3dd81d370973
[cb02]: https://medium.com/swlh/setup-a-chromebook-for-web-development-14c2c2e1a24b
[cb03]: https://stackoverflow.com/questions/43743509/how-to-make-python3-command-run-python-3-6-instead-of-3-5


#### [1.B.1] Update the build environment

```bash
sudo apt-get update;
sudo apt-get upgrade;
```
#### [1.B.2] Install the build tools

```bash
sudo apt-get install build-essential;
sudo apt-get install libreadline-gplv2-dev libncursesw5-dev;
sudo apt-get instsall libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev;
sudo apt-get install libffi-dev liblzma-dev;
```

#### [1.B.3] Download Python, build it and install it.

```bash
cd /usr/src;
sudo wget https://www.python.org/ftp/python/3.8.9/Python-3.8.9.tgz;
sudo tar xzf Python-3.8.9.tgz;
cd Python-3.8.9;
sudo ./configure --enable-optimizations;
sudo make altinstall;
```

#### If you feel wild.. but do not recommend
```bash
sudo rm /usr/bin/python3;
sudo ln -s /usr/local/bin/python3.8 /usr/bin/python3;
```

***RESET/ UNDO***: ```sudo ln -s /usr/bin/python3.7 /usr/bin/python3;```

## [2] Install Python pip

Now that you have the correct version of Python install (linux & chromebook),
it is time to install the python package manager called 'pip'.

install pip, upgrade pip, install tweepy via pip and check version of tweepy
```bash
sudo apt-get install python3-pip;
python3.8 -m pip install --upgrade pip; 
```

## [3] Social Network Scraper - snscrape

### [3.A] Using scraper for tweets
The tool [Social Network Scraper][sn01] is created by [Jason Scott][js01]. 
Jason is known for creating the internet archive.

Martin Beck has written a few blogs, and provided some example code, 
on how to download and analyse tweets using snscrape.

1. [How to Scrape Tweets With snscrape by Martin Beck][sn02]
1. [Example code how to use snscrape via the command line, on Github by Martin Beck][sn03-cli]
1. [Example code how to use snscrape in pyton, on Github by Martin Beck][sn03-python]


### [3.B] Install snscrape

Snscrape is installed via python. 
Once installed you can use snscrape via the command line 
and call it in your python scripts.

Important is that you need the "developer" version of this scraper,
since many functions are not provided by the "official" release.

```bash
python3.8 -m pip install git+https://github.com/JustAnotherArchivist/snscrape.git;
python3.8 -m pip freeze|grep snscrape;
python3.8 -m pip show snscrape;
```

### [3.C] Test snscrape from command line

You can test snscrape, by calling it from the command line.
This command will download 100 tweets from the twitter user '@textfiles'
and write them into the file 'textfiles'.

```bash
snscrape --max-results 100 twitter-user textfiles >twitter-@textfiles;
```

### [3.D] Use snscrape in Python

#### [3.D.1] Install pandas
```bash
python3.8 -m pip install pandas;
```

#### [3.D.2] Create script
This code is from [Scraping Tweets by Location in Python using snscrape by Scott Tomlins][sn05].
This code searches twitter for tweets containing the words 'data science'.
It limits the search to the first '100' results.
The search result that is returned by snscrape is limited to the columns 'date' and 'content'
and displayed in your screen.
 
```python
# https://medium.com/swlh/how-to-scrape-tweets-by-location-in-python-using-snscrape-8c870fa6ec25
import pandas as pd
import snscrape.modules.twitter as sntwitter
import itertools

# our search term, using syntax for Twitter's Advanced Search
search = '"data science"'

# the scraped tweets, this is a generator
scraped_tweets = sntwitter.TwitterSearchScraper(search).get_items()

# slicing the generator to keep only the first 100 tweets
sliced_scraped_tweets = itertools.islice(scraped_tweets, 100)

# convert to a DataFrame and keep only relevant columns
df = pd.DataFrame(sliced_scraped_tweets)[['date', 'content']]

pd.DataFrame(itertools.islice(sntwitter.TwitterSearchScraper(
    '"data science"').get_items(), 100))
prinf(df);
```

### More examples
#### 'snscrape-user-1.py'

Example code that downloads 5 tweets from the search result 'data science',
and then puts the result into JSON, Panda's (dataframe), and in a CSV file.
So that you can select and re-use.

The channenge here is that the JSON return of snscrape includes a datetime attribute
on which the function ```json.dumps()``` chocked. 

On the page [How to serialize a datetime object as JSON using Python? by Gabor Szabo][sn18]
an example was provided how to 'clean' the datetume attribute, by defining the helper function 
```myconverter```.


```python
from test_internet import only_connected
only_connected()
# example code: https://github.com/MartinBeckUT/TwitterScraper/blob/master/snscrape/python-wrapper/snscrape-python-wrapper.py
# library: https://github.com/JustAnotherArchivist/snscrape
# CC BY-SA 4.0

import pandas as pd
import snscrape.modules.twitter as sntwitter
import itertools

import csv
import os.path

import json
import datetime

print("--------------------------")
print("Set csv file location")
yymmddhhss   = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
csv_filename = "snscrape-user_" + yymmddhhss + ".csv"
dirsname = os.path.join('data')
if not os.path.exists(dirsname):
    os.makedirs(dirsname)
csvlogPath = os.path.join(dirsname,csv_filename)

print("--------------------------")
print("Set max tweetsearch return and tweestsearch query")
itemsPer     = 5
search       = '"data science"'

print("--------------------------")
print("Get tweets and store in s")
    ### get dataframe
df = pd.DataFrame(itertools.islice(
    sntwitter.TwitterSearchScraper(
            search).get_items(), itemsPer))
    ### get sub-set of the columns of what TwitterSearchScraper returned
twitter_users = df[['user','id','url']]
    ### get only the column user from the previous sub-set
twitter_users_username_1 = twitter_users[['user']]
    ### story only one entry of all the users to play with
s  = twitter_users_username_1['user'][0]
print(s)
print(type(s))

print("--------------------------")
print("Dump (with converter) json s to string x")
def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()
x = json.dumps(s, default = myconverter)
print(x)

print("--------------------------")
print("Load string x into dictionary s")
s = json.loads(x)
print(s)

print("--------------------------")
print("Load load json s into panda q")
q = pd.DataFrame.from_dict(s, orient='index')
q.reset_index(level=0, inplace=True)
print(q.head())

print("--------------------------")
print("Store panda q into csvlogPath f")
q.to_csv(csvlogPath, sep=',', quoting=csv.QUOTE_ALL, index=False)
print("Printing ["+csvlogPath+"]")
f = open(csvlogPath)
csv_f = csv.reader(f)
for row in csv_f:
  print(row)

print("--------------------------")
print("done")

```

#### 'snscrape-user-2.py'
Example code that downloads 5 tweets from the search result 'data science',
and then puts the result into a list and in a CSV file.
So that you can select and re-use.

```python
from test_internet import only_connected
only_connected()

# example code: https://github.com/MartinBeckUT/TwitterScraper/blob/master/snscrape/python-wrapper/snscrape-python-wrapper.py
# library: https://github.com/JustAnotherArchivist/snscrape
# CC BY-SA 4.0

import pandas as pd
import snscrape.modules.twitter as sntwitter
import itertools

import csv
import datetime

## search settings
tweet_list   = []
itemsPer     = 5
search       = '"data science"'

## CSV settings
yymmddhhss   = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
csv_filename = 'snscrape-user_' + yymmddhhss + '.csv'

### get list
for i, tweet in enumerate(
        itertools.islice(
            sntwitter.TwitterSearchScraper(
                search).get_items(), itemsPer)
    ):
        #select tweet data
        tweet_list.append([tweet.date, tweet.id, 
            tweet.content, tweet.user.username])

### put list into dataframe
tweet_df = pd.DataFrame(tweet_list, 
    columns=['Datetime', 'Tweet Id', 'Text', 'Username'])

### put dataframe ito csv
tweet_df.to_csv(csv_filename, sep=',', quoting=csv.QUOTE_ALL, index=False)

### print csv
print("Printing ["+csv_filename+"]")
f = open(csv_filename)
csv_f = csv.reader(f)
for row in csv_f:
  print(row)


```

## Check internet helper script
To download tweets, and to search for tweets you need an active internet connection.
The timeout of snscrape can be a few seconds,
therefore there is the 'test_internet.py' script.
This script is based upon code from [Checking network connection @ Stack overflow][ci01].

- This script wil print "We are connected. conntinue." when there is an active internet connection
- This script wil print "We are not connected. exit" and exit your script when tere is not an active internet connection.


```python
def is_connected(hostname):
  import socket

    #https://stackoverflow.com/questions/3764291/checking-network-connection
  try:
    # see if we can resolve the host name -- tells us if there is
    # a DNS listening
    host = socket.gethostbyname(hostname)
    # connect to the host -- tells us if the host is actually
    # reachable
    s = socket.create_connection((host, 80), 2)
    s.close()
    return True
  except:
     pass
  return False

def only_connected():
  REMOTE_SERVER = "one.one.one.one"
  if not(is_connected(REMOTE_SERVER)):
      print("We are not connected. exit.")
      exit()
  else :
      print("We are connected. conntinue")

```


[ci01]: https://stackoverflow.com/questions/3764291/checking-network-connection


## References

### Some sites on how to download many tweets
[js01]: https://en.wikipedia.org/wiki/Jason_Scott

[sn01]: https://github.com/JustAnotherArchivist/snscrape
[sn02]: https://betterprogramming.pub/how-to-scrape-tweets-with-snscrape-90124ed006af
[sn03-cli]: https://github.com/MartinBeckUT/TwitterScraper/tree/master/snscrape/cli-with-python
[sn03-python]: https://github.com/MartinBeckUT/TwitterScraper/blob/master/snscrape/python-wrapper/snscrape-python-wrapper.py
[sn04]: https://github.com/JustAnotherArchivist/snscrape/issues/76

[sn05]: https://medium.com/swlh/how-to-scrape-tweets-by-location-in-python-using-snscrape-8c870fa6ec25
[sn06]: https://blogs.embarcadero.com/web-scraping-6-ways-to-rapidly-collect-massive-datasets-in-your-windows-apps/
[sn07]: https://medium.com/dataseries/how-to-scrape-millions-of-tweets-using-snscrape-195ee3594721
[sn08]: https://stackoverflow.com/questions/24214189/how-can-i-get-tweets-older-than-a-week-using-tweepy-or-other-python-libraries

[sn09]: https://medium.datadriveninvestor.com/how-to-build-a-twitter-scraping-app-with-python-b3fc069a19c6 
[sn10]: https://github.com/cedoard/snscrape_twitter
[sn11]: https://gist.dreamtobe.cn/cedoard/6b8f8e1ffbeb8aa3caadd898cb67d8f3
[sn12]: https://www.reddit.com/r/learnpython/comments/jlgmnm/scraping_tweets_using_snscrape/ 

[sn13]: https://www.atoti.io/how-im-failing-my-twitter-sentiment-analysis-for-cryptocurrency-prediction/
[sn14]: https://zhuanlan.zhihu.com/p/342441381
[sn15]: https://www.programmersought.com/article/76694534600/
[sn16]: https://gist.github.com/anku255/0cebd75cce675f2b56de1ef48ec06575
[sn17]: https://www.kaggle.com/prathamsharma123/clean-raw-json-tweets-data
[sn18]: https://code-maven.com/serialize-datetime-object-as-json-in-python


1. [Scraping Tweets by Location in Python using snscrape by Scott Tomlins][sn05]
1. [How to Build a Twitter Scraping App with Python by By Graham Sahagian][sn09]
1. [6 Ways To Rapidly Collect Massive Datasets in your Apps by Muhammad Azizul Hakim][sn06]
1. [How to scrape millions of tweets using snscrape by Rashi Desai][sn07]
1. [Using snscrape and tweepy libraries to scrape unlimited amount of tweets][sn10]
1. [How can I get tweets older than a week (using tweepy or other python libraries) @ Stack Overflow][sn08], including the usage of a Sentiment Analyser
1. user cedoard posted on [Reddit][sn12] that he created a tweepy and snscrape exaple code on [Github][sn10], including a [Github Gist][sn11]
1. [Using Twitter to forecast cryptocurrency returns #1 – How to scrape Twitter for sentiment analysis By Hui Fang Yeo][sn13]
1. [Twitter API：tweepy & snscrape by 一条卷筒粉][sn14]
1. [anku255/scrapeTweets.py][sn16] Github Gist code using tweepy to download tweets and storing is CSV (with delimeter)
1. [Cleaning raw JSON tweets data scraped using snscrape library by Pratham Sharma][sn17]
1. [How to serialize a datetime object as JSON using Python? by Gabor Szabo][sn18]


### Python CSV, PANDA's and JSON
[py03]: https://www.programmersought.com/article/76694534600/
[py04]: https://codingnetworker.com/2015/10/python-dictionaries-json-crash-course/
[py05]: https://docs.python.org/3/library/csv.html#dialects-and-formatting-parameters
[py06]: https://stackoverflow.com/questions/46638630/importing-tweets-to-csv-file-using-python
[py07]: https://stackoverflow.com/questions/57454973/parsing-a-text-file-with-tweets-to-csv-with-delimiter
[py08]: https://stackoverflow.com/questions/46455939/pandas-fast-way-of-accessing-a-column-of-objects-attribute

1. [Pandas - Fast way of accessing a column of objects' attribute @ Stack Overflow][py08]
1. [csv — CSV File Reading and Writing @ Python.org][py05]
1. [importing tweets to csv file using Python @ Stack Overflow][py06]
1. [Parsing a text file with tweets to csv with '|' delimiter @ Stack overflow][py07] 
1. [Python's common way to serialize objects (pickle, shelve, json)][py03]
1. [python dictionaries and JSON (crash course) @ Coding Network][py04]




## Tweepy
Since many examples on how to download tweets use Tweepy, here some basic information.

### Homepage of Tweepy
https://github.com/tweepy/tweepy

### Tweepy versions in regards to Twitter API
- The old version of tweepy (v3) 
talks to the old version of the twitter api (v1).

- The new version of tweepy (v4)
talks to the new version of the twitter api (v2).

Use tweepy v3.10 since this is the stable version.
Tweepy 4.0 is using the twitter API v2.0, and is not yet fully developed and documented.

### install pip, upgrade pip, install tweepy via pip and check version of Tweepy
```bash
sudo apt-get install python3-pip;
python3.8 -m pip install --upgrade pip; 
python3.8 -m pip install tweepy;
python3.8 -m pip freeze|grep tweepy; 
```
# License
CC BY-SA 4.0
