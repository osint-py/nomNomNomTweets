from test_internet import only_connected
only_connected()
# example code: https://github.com/MartinBeckUT/TwitterScraper/blob/master/snscrape/python-wrapper/snscrape-python-wrapper.py
# library: https://github.com/JustAnotherArchivist/snscrape
# CC BY-SA 4.0

import pandas as pd
import snscrape.modules.twitter as sntwitter
import itertools

import csv
import os.path

import json
import datetime

print("--------------------------")
print("Set csv file location")
yymmddhhss   = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
csv_filename = "snscrape-user_" + yymmddhhss + ".csv"
dirsname = os.path.join('data')
if not os.path.exists(dirsname):
    os.makedirs(dirsname)
csvlogPath = os.path.join(dirsname,csv_filename)

print("--------------------------")
print("Set max tweetsearch return and tweestsearch query")
itemsPer     = 5
search       = '"data science"'

print("--------------------------")
print("Get tweets and store in s")
    ### get dataframe
df = pd.DataFrame(itertools.islice(
    sntwitter.TwitterSearchScraper(
            search).get_items(), itemsPer))
    ### get sub-set of the columns of what TwitterSearchScraper returned
twitter_users = df[['user','id','url']]
    ### get only the column user from the previous sub-set
twitter_users_username_1 = twitter_users[['user']]
    ### story only one entry of all the users to play with
s  = twitter_users_username_1['user'][0]
print(s)
print(type(s))

print("--------------------------")
print("Dump (with converter) json s to string x")
def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()
x = json.dumps(s, default = myconverter)
print(x)

print("--------------------------")
print("Load string x into dictionary s")
s = json.loads(x)
print(s)

print("--------------------------")
print("Load load json s into panda q")
q = pd.DataFrame.from_dict(s, orient='index')
q.reset_index(level=0, inplace=True)
print(q.head())

print("--------------------------")
print("Store panda q into csvlogPath f")
q.to_csv(csvlogPath, sep=',', quoting=csv.QUOTE_ALL, index=False)
print("Printing ["+csvlogPath+"]")
f = open(csvlogPath)
csv_f = csv.reader(f)
for row in csv_f:
  print(row)

print("--------------------------")
print("done")