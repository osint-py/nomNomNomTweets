from test_internet import only_connected
only_connected()

# example code: https://github.com/MartinBeckUT/TwitterScraper/blob/master/snscrape/python-wrapper/snscrape-python-wrapper.py
# library: https://github.com/JustAnotherArchivist/snscrape
# CC BY-SA 4.0

import pandas as pd
import snscrape.modules.twitter as sntwitter
import itertools

import csv
import datetime

## search settings
tweet_list   = []
itemsPer     = 5
search       = '"data science"'

## CSV settings
yymmddhhss   = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
csv_filename = 'snscrape-user_' + yymmddhhss + '.csv'

### get list
for i, tweet in enumerate(
        itertools.islice(
            sntwitter.TwitterSearchScraper(
                search).get_items(), itemsPer)
    ):
        #select tweet data
        tweet_list.append([tweet.date, tweet.id, 
            tweet.content, tweet.user.username])

### put list into dataframe
tweet_df = pd.DataFrame(tweet_list, 
    columns=['Datetime', 'Tweet Id', 'Text', 'Username'])

### put dataframe ito csv
tweet_df.to_csv(csv_filename, sep=',', quoting=csv.QUOTE_ALL, index=False)

### print csv
print("Printing ["+csv_filename+"]")
f = open(csv_filename)
csv_f = csv.reader(f)
for row in csv_f:
  print(row)