def is_connected(hostname):
  import socket

    #https://stackoverflow.com/questions/3764291/checking-network-connection
  try:
    # see if we can resolve the host name -- tells us if there is
    # a DNS listening
    host = socket.gethostbyname(hostname)
    # connect to the host -- tells us if the host is actually
    # reachable
    s = socket.create_connection((host, 80), 2)
    s.close()
    return True
  except:
     pass
  return False

def only_connected():
  REMOTE_SERVER = "one.one.one.one"
  if not(is_connected(REMOTE_SERVER)):
      print("We are not connected. exit.")
      exit()
  else :
      print("We are connected. conntinue")